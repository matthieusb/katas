# katas

This repository stores miscellaneous (partially) solved katas used during dojos, code retreats or personal work.
Most of them take inspiration from [Emily Bache's repository](https://github.com/emilybache) and "The coding dojo handbook".

If you are looking for starter code for these katas, have a look at my [kata-starters repository](https://gitlab.com/matthieusb/kata-starters)
